#pragma once

// FIXME
#ifdef __APPLE__
#define GL_SILENCE_DEPRECATION 
#endif

#ifndef NOMINMAX
#define NOMINMAX
#endif

#include <glad/gl.h>
#include <GLFW/glfw3.h>
#include <imgui.h>
#include <implot.h>
#include <map>
#include <string>

#include "fonts/Fonts.h"

/// Macro to disable console on Windows
#if defined(_WIN32) && defined(APP_NO_CONSOLE)
#pragma comment(linker, "/subsystem:windows /ENTRY:mainCRTStartup")
#endif

struct app {
    app(std::string title, int w, int h, int argc, char const* argv[]);
    virtual ~app();
    // Called at top of run
    virtual void start() {}
    // Update, called once per frame.
    virtual void update()
    { /*implement me*/
    }
    virtual void finish() = 0;
    // Runs the app.
    void run();
    // Get window size
    ImVec2 get_window_size() const;

    ImVec4 ClearColor;                    // background clear color
    GLFWwindow* Window;                   // GLFW window handle
    std::map<std::string, ImFont*> Fonts; // font map
    bool UsingDGPU;                      // using discrete gpu (laptops only)
};

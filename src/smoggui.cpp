#include "app.h"

#include <zmq.hpp>
#include <iostream>
#include <thread>

bool finishing;

struct smoggui : public app {
    using app::app;

    void start() override
    {

    }

    void finish() override
    {

    }
    
    void zmq_read_func(int zmq_id, const std::string &zmq_addr)
    {
        zmq::context_t ctx;
        zmq::socket_t subscriber(ctx, ZMQ_SUB);

        std::cerr << "zmq_read_func: Thread started with ZMQ address " << zmq_addr
                  << std::endl;

        subscriber.connect("tcp://" + zmq_addr + ":7601");
#if CPPZMQ_VERSION >= ZMQ_MAKE_VERSION(4, 8, 0)
        subscriber.set(zmq::sockopt::subscribe, "");
#else
        subscriber.setsockopt(ZMQ_SUBSCRIBE, "");
#endif

        zmq::message_t msg;

        while (!finishing) {

#if CPPZMQ_VERSION >= ZMQ_MAKE_VERSION(4, 3, 1)
            zmq::recv_result_t zmq_res = subscriber.recv(msg, zmq::recv_flags::dontwait);
#else
            bool zmq_res = subscriber.recv(&msg);
#endif

            if (!zmq_res) {
                std::this_thread::sleep_for(std::chrono::milliseconds(100));
                continue;
            }
        }

        std::cerr << "zmq_read_func: Thread with address " << zmq_addr << " terminated."
                  << std::endl;
    }

};

int main(int argc, char const* argv[])
{
    // int screen_font_size = 22;

    smoggui app("MRC-100", 960, 540, argc, argv);
    app.run();
}
